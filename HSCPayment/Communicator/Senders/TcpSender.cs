using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using Communicator.Exceptions;
using Communicator.Interfaces;
using FiscalProto.TCP;
using static Communicator.Services.ReaderService;

namespace Communicator.Senders
{
    public class TcpSender : ISender 
    {
        private readonly string _address;
        private readonly int _port;

        public TcpSender(string address, int port)
        {
            _address = address;
            _port = port;
        }
        public TcpMessage Send(TcpMessage tcpMessage, long internalTransactionTimeout)
        {
            if (internalTransactionTimeout <= 0L)
                throw new TransactionTimeoutException();
            using TcpClient tcpClient = new TcpClient();
            ManualResetEventSlim manualResetEventSlim = new ManualResetEventSlim(false);
            IAsyncResult asyncResult;
            try
            {
                asyncResult = tcpClient.BeginConnect(_address, _port, RequestCallback, new AsyncState()
                {
                    Client = tcpClient,
                    Message = tcpMessage,
                    TransactionHandler = manualResetEventSlim
                });
            }
            catch (Exception ex)
            {
                throw new ConnectionException(ex);
            }

            if (!manualResetEventSlim.Wait(TimeSpan.FromMilliseconds(internalTransactionTimeout)))
                throw new TransactionTimeoutException();
            AsyncState asyncState = (AsyncState) asyncResult.AsyncState;
            if (asyncState.Exception != null)
                throw asyncState.Exception;
            return asyncState.Message;
        }

        private void RequestCallback(IAsyncResult result)
        {
            AsyncState asyncState = (AsyncState) result.AsyncState;
            TcpClient client = asyncState.Client;
            TcpMessage message = asyncState.Message;
            
            try
            {
                using NetworkStream stream = client.GetStream();
                byte[] package = message.GetPackage();
                WriteBytes(stream, package);
                byte[] numArray = new byte[Marshal.SizeOf(typeof (TcpHeader))];
                ReadBytes(stream, numArray);
                TcpHeader tcpHeader = numArray.Deserialize();
                byte[] target = new byte[tcpHeader.Size - numArray.Length];
                ReadBytes(stream, target);
                asyncState.Message = new TcpMessage
                {
                    TcpHeader = tcpHeader,
                    Payload = target
                };
            }
            catch (CommunicationException ex)
            {
                asyncState.Exception = ex;
            }
            catch (Exception ex)
            {
                asyncState.Exception = new ConnectionException(ex);
            }
            finally
            {
                asyncState.TransactionHandler.Set();
            }
        }
        
        
        private class AsyncState
        {
            public TcpClient Client;
            public TcpMessage Message;
            public Exception Exception;
            public ManualResetEventSlim TransactionHandler;
        }
    }
}