using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Communicator.Exceptions;
using FiscalProto.KkmProto;
using FiscalProto.KkmProto.Enums;
using FiscalProto.TCP;
using FiscalProto.TCP.OFD;

namespace Communicator.Senders
{
    public class TcpSenderWrapper

    {
        private readonly TcpSender _tcpSender;
        private readonly long _transactionTimeout;
        private List<ResultTypeEnum> _resultCodesToResend;

        public TcpSenderWrapper(string address, int port, int transactionTimeout)
        {
            _tcpSender = new TcpSender(address, port);
            _transactionTimeout = transactionTimeout * 1000;
        }

        public TcpSenderWrapper SetRespondCodesToResend(List<ResultTypeEnum> resultCodes)
        {
            _resultCodesToResend = resultCodes;
            return this;
        }

        public OFDResponse Send(OFDRequest source)
        {
            Stopwatch timer = Stopwatch.StartNew();
            return SendInternal(source, timer);
        }

        private OFDResponse SendInternal(OFDRequest source, Stopwatch timer)
        {
            try
            {
                long internalTransactionTimeout = _transactionTimeout - timer.ElapsedMilliseconds;
                TcpMessage tcpMessage = _tcpSender.Send(source.PrepareMessage(), internalTransactionTimeout);
                Response response = tcpMessage.ExtractResponse();
                if (!IsSatisfiedResponse(response))
                    throw new SendException();
                return new OFDResponse()
                {
                    OfdInfo = UpdateOFDInfo(source.OFDInfo, tcpMessage.TcpHeader),
                    Response = response
                };
            }
            catch (SendException ex)
            {
                Thread.Sleep(500);
                return SendInternal(source, timer);
            }
        }

        private bool IsSatisfiedResponse(Response response)
        {
            if (response.Result == null || _resultCodesToResend == null)
                return true;
            return !_resultCodesToResend.Contains((ResultTypeEnum) response.Result.Code);
        }

        private static OFDInfo UpdateOFDInfo(OFDInfo ofdInfo, TcpHeader tcpHeader)
        {
            ofdInfo.Token = tcpHeader.Token;
            ofdInfo.ReqNum = (uint) ((int) ushort.MaxValue == (int) tcpHeader.ReqNum
                ? 1
                : (int) (ushort) (tcpHeader.ReqNum + 1U));
            return ofdInfo;
        }
    }
}