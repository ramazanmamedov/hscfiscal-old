using FiscalProto.TCP;

namespace Communicator.Interfaces
{
    public interface ISender
    { 
        TcpMessage Send(TcpMessage tcpMessage, long internalTransactionTimeout);
    }
}