﻿using System;

namespace Communicator.Exceptions
{
    public class TransactionTimeoutException : CommunicationException
    {
        public TransactionTimeoutException()
        {
        }

        public TransactionTimeoutException(Exception exception)
            : base(nameof(TransactionTimeoutException), exception)
        {
        }
    }
}