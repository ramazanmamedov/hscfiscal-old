﻿
 using System;

 namespace Communicator.Exceptions
{
  public class ConnectionException : CommunicationException
  {
    public ConnectionException()
    {
    }

    public ConnectionException(Exception exception)
      : base(nameof (ConnectionException), exception)
    {
    }
  }
}
