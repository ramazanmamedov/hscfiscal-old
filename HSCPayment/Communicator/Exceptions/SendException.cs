﻿
 using System;

 namespace Communicator.Exceptions
{
  public class SendException : CommunicationException
  {
    public SendException()
    {
    }

    public SendException(Exception exception)
      : base(nameof (SendException), exception)
    {
    }
  }
}
