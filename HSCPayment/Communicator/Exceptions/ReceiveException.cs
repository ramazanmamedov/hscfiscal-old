﻿
 using System;

 namespace Communicator.Exceptions
{
  public class ReceiveException : CommunicationException
  {
    public ReceiveException(Exception exception)
      : base(nameof (ReceiveException), exception)
    {
    }
  }
}
