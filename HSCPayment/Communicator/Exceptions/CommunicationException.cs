﻿using System;

 namespace Communicator.Exceptions
{
  public abstract class CommunicationException : Exception
  {
    protected CommunicationException()
    {
    }

    protected CommunicationException(string message)
      : base(message)
    {
    }

    protected CommunicationException(Exception exception)
      : base("CommunicationException ", exception)
    {
    }

    protected CommunicationException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
