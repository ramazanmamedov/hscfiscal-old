using System;
using System.Net.Sockets;
using Communicator.Exceptions;

namespace Communicator.Services
{
    public static class ReaderService
    {
        public static void WriteBytes(NetworkStream stream, byte[] source)
        {
            try
            {
                stream.Write(source, 0, source.Length);
            }
            catch (Exception ex)
            {
                throw new SendException(ex);
            }
        }

        public static void ReadBytes(NetworkStream stream, byte[] target)
        {
            try
            {
                int offset = 0;
                while (offset < target.Length)
                {
                    int num = stream.Read(target, offset, target.Length - offset);
                    offset += num;
                }
            }
            catch (Exception ex)
            {
                throw new ReceiveException(ex);
            }
        }
    }
}