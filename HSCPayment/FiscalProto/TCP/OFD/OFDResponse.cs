using FiscalProto.KkmProto;

namespace FiscalProto.TCP.OFD
{
    public class OFDResponse
    {
        public Response Response { get; set; }

        public OFDInfo OfdInfo { get; set; }
    }
}