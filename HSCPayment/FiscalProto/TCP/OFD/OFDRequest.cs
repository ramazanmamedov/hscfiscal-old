using FiscalProto.KkmProto;

namespace FiscalProto.TCP.OFD
{
    public class OFDRequest
    {
        public Request Request { get; set; }
        public OFDInfo OFDInfo { get; set; }
    }
}