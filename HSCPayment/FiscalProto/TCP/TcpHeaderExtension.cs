using System;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace FiscalProto.TCP
{
    public static class TcpHeaderExtension
    {
        public static byte[] Serialize(this TcpHeader tcpHeader)
        {
            int length = Marshal.SizeOf(tcpHeader);
            byte[] target = new byte[length];
            IntPtr destination = Marshal.AllocHGlobal(length);
            Marshal.StructureToPtr(tcpHeader, destination, true);
            Marshal.Copy(destination, target, 0, length);
            Marshal.FreeHGlobal(destination);
            MaybeAdjustEndianness(typeof (TcpHeader), target, true, 0);
            return target;
        }
        
        
        public static TcpHeader Deserialize(this byte[] source)
        {
            int length = Marshal.SizeOf(typeof (TcpHeader));
            IntPtr destination = Marshal.AllocHGlobal(length);
            MaybeAdjustEndianness(typeof (TcpHeader), source, true, 0);
            Marshal.Copy(source, 0, destination, length);
            TcpHeader tcpHeader = (TcpHeader) Marshal.PtrToStructure(destination, typeof (TcpHeader));
            Marshal.FreeHGlobal(destination);
            return tcpHeader;
        }
        
        
        private static void MaybeAdjustEndianness(Type type, byte[] data, bool isLittleEndian, int startOffset = 0)
        {
            if (BitConverter.IsLittleEndian == isLittleEndian)
                return;
            foreach (FieldInfo field in type.GetFields())
            {
                Type fieldType = field.FieldType;
                if (field.IsStatic || fieldType == typeof(string)) continue;
                int int32 = Marshal.OffsetOf(type, field.Name).ToInt32();
                FieldInfo[] array = fieldType.GetFields().Where(subField => !subField.IsStatic).ToArray();
                int num = startOffset + int32;
                if (array.Length == 0)
                    Array.Reverse(data, num, Marshal.SizeOf(fieldType));
                else
                    MaybeAdjustEndianness(fieldType, data, isLittleEndian, num);
            }
        }
    }
}