using System.Runtime;

namespace FiscalProto.TCP
{
    public class TcpMessage
    {
        public TcpHeader TcpHeader { get; set; }
        public byte[] Payload { get; set; }
    }
}