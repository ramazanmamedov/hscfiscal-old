using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using FiscalProto.KkmProto;
using FiscalProto.TCP.OFD;
using ProtoBuf;

namespace FiscalProto.TCP
{
    public static class TcpMessageExtension
    {
        public static Response ExtractResponse(this TcpMessage source)
        {
            using MemoryStream memoryStream = new MemoryStream(source.Payload) {Position = 0L};
            return Serializer.Deserialize<Response>(memoryStream);
        }

        public static TcpMessage PrepareMessage(this OFDRequest source)
        {
            using MemoryStream memoryStream = new MemoryStream();
            Serializer.Serialize(memoryStream, source.Request);
            byte[] payload = memoryStream.ToArray();
            return new TcpMessage()
            {
                TcpHeader = new TcpHeader()
                {
                    AppCode = 33186,
                    Size = (uint) (payload.Length + Marshal.SizeOf(typeof (TcpHeader))),
                    Id = source.OFDInfo.SystemId,
                    Token = source.OFDInfo.Token,
                    ReqNum = (ushort) source.OFDInfo.ReqNum,
                    Version = 125
                },
                Payload = payload
            };
        }

        public static byte[] GetPackage(this TcpMessage source)
        {
            return source.TcpHeader.Serialize().Concat(source.Payload).ToArray();
        }
    }
}