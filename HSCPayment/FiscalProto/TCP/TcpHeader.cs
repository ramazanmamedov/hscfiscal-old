using System.Runtime.InteropServices;

namespace FiscalProto.TCP
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct TcpHeader
    {
        public ushort AppCode;
        public ushort Version;
        public uint Size;
        public uint Id;
        public uint Token;
        public ushort ReqNum;
    }
}