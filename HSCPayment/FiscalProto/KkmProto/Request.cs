using System.ComponentModel;
using FiscalProto.KkmProto.Enums;
using FiscalProto.KkmProto.Report;
using FiscalProto.KkmProto.Service;
using FiscalProto.KkmProto.Ticket;
using ProtoBuf;

namespace FiscalProto.KkmProto
{
    [ProtoContract(Name = "Request")]
    public class Request
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "command")]
        public CommandTypeEnum Command { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "ticket")]
        [DefaultValue(null)]
        public TicketRequest Ticket { get; set; }
        
        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "close_shift")]
        [DefaultValue(null)]
        public CloseShiftRequest CloseShift { get; set; }
        
        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "report")]
        [DefaultValue(null)]
        public ReportRequest Report { get; set; }
        
        [DefaultValue(null)]
        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "nomenclature")]
        public NomenclatureRequest Nomenclature { get; set; }
        
        [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "service")]
        [DefaultValue(null)]
        public ServiceRequest Service { get; set; }
        
        [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "money_placement")]
        [DefaultValue(null)]
        public MoneyPlacementRequest MoneyPlacement { get; set; }
        
        [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = false, Name = "auth")]
        [DefaultValue(null)]
        public AuthRequest Auth { get; set; }
    }
    
}