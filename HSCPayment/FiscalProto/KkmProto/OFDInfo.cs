namespace FiscalProto.KkmProto
{
    public class OFDInfo
    {
        public uint SystemId { get; set; }
        public uint Token { get; set; }
        public uint ReqNum { get; set; }
    }
}