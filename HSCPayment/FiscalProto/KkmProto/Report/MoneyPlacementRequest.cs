using System;
using System.ComponentModel;
using FiscalProto.KkmProto.Common;
using FiscalProto.KkmProto.Enums;
using ProtoBuf;
using DateTime = FiscalProto.KkmProto.DateTimeProto.DateTime;

namespace FiscalProto.KkmProto.Report
{
    [ProtoContract(Name = "CloseShiftRequest")]
    [Serializable]
    public class MoneyPlacementRequest
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "datetime")]
        public DateTime DateTime { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "operation")]
        public MoneyPlacementEnum Operation { get; set; }

        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "sum")]
        public Money Sum { get; set; }

        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "is_offline")]
        [DefaultValue(false)]
        public bool IsOffline { get; set; }

        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "fr_shift_number")]
        [DefaultValue(0)]
        public uint FrShiftNumber { get; set; }

        [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "shift_document_number")]
        [DefaultValue(0)]
        public uint ShiftDocumentNumber { get; set; }
    }
}