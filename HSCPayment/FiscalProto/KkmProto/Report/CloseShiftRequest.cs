using System;
using System.ComponentModel;
using FiscalProto.KkmProto.Ticket;
using ProtoBuf;
using DateTime = FiscalProto.KkmProto.DateTimeProto.DateTime;

namespace FiscalProto.KkmProto.Report
{
    [ProtoContract(Name = "CloseShiftRequest")]
    [Serializable]
    public class CloseShiftRequest
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "close_time")]
        public DateTime CloseTime { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "is_offline")]
        [DefaultValue(false)]
        public bool IsOffline { get; set; }

        [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "fr_shift_number")]
        [DefaultValue(0)]
        public uint FsShiftNumber { get; set; }

        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "withdraw_money")]
        [DefaultValue(false)]
        public bool WithdrawMoney { get; set; }

        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "z_report")]
        [DefaultValue(null)]
        public ZXReport ZxReport { get; set; }

        [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "operator")]
        [DefaultValue(null)]
        public Operator Operator { get; set; }

        [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "shift_document_number")]
        [DefaultValue(0)]
        public uint ShiftDocumentNumber { get; set; }
    }
}