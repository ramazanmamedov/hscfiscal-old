using System;
using FiscalProto.KkmProto.Common;
using FiscalProto.KkmProto.Enums;
using ProtoBuf;

namespace FiscalProto.KkmProto.Report
{
    [ProtoContract(Name = "TaxOperation")]
    [Serializable]
    public class TaxOperation
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "operation")]
        public OperationTypeEnum Operation { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "turnover")]
        public Money TurnOver { get; set; }

        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "sum")]
        public Money Sum { get; set; }
    }
}