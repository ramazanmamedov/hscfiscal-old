using System;
using System.Collections.Generic;
using ProtoBuf;

namespace FiscalProto.KkmProto.Report
{
    [ProtoContract(Name = "Tax")]
    [Serializable]
    public class Tax
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "tax_type")]
        public uint TaxType { get; set; }

        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "percent")]
        public uint Percent { get; set; }

        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "operations")]
        public List<TaxOperation> Operations { get; set; }
    }
}