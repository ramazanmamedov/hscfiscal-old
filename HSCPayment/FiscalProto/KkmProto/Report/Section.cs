using System;
using System.Collections.Generic;
using ProtoBuf;

namespace FiscalProto.KkmProto.Report
{
    [ProtoContract(Name = "Section")]
    [Serializable]
    public class Section
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "section_code")]
        public string SectionCode { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "operations")]
        public List<Operation> Operations { get; set; }
    }
}