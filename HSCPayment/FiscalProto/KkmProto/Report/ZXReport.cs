using System.Collections.Generic;
using FiscalProto.KkmProto.DateTimeProto;

namespace FiscalProto.KkmProto.Report
{
    public class ZXReport
    {
        public DateTime DateTime { get; set; }

        public uint ShiftNumber { get; set; }

        public List<Section> Sections { get; set; }

        public List<Operation> Operations { get; set; }
        
        public List<Operation> Discounts { get; set; }
        
        public List<Operation> Markups { get; set; }
        
        public List<Operation> TotalResult { get; set; }
    }
}