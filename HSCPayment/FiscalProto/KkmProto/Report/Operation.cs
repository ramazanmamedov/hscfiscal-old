using System;
using FiscalProto.KkmProto.Common;
using FiscalProto.KkmProto.Enums;
using ProtoBuf;

namespace FiscalProto.KkmProto.Report
{
    [ProtoContract(Name = "Operation")]
    [Serializable]
    public class Operation
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "operation")]
        public OperationTypeEnum OperationType { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "count")]
        public uint Count { get; set; }

        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "sum")]
        public Money Sum { get; set; }
    }
}