using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto
{
    [ProtoContract(Name = "Result")]
    [Serializable]
    public class Result
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "result_code")]
        public uint Code { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "result_text")]
        public string Text { get; set; }
    }
}