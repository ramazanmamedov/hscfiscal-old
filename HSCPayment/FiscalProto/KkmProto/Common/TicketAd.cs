using System;
using ProtoBuf;

namespace FiscalProto.KkmProto.Common
{
    [ProtoContract(Name = "TicketAd")]
    [Serializable]
    public class TicketAd
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "info")]
        public TicketAdInfo Info { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "text")]
        public string Text { get; set; }
    }
}