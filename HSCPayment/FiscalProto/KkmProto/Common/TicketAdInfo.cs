using System;
using FiscalProto.KkmProto.Enums;
using ProtoBuf;

namespace FiscalProto.KkmProto.Common
{
    [ProtoContract(Name = "TicketAdInfo")]
    [Serializable]
    public class TicketAdInfo
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "type")]
        public TicketAdTypeEnum Type { get; set; }
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "version")]
        public ulong Version { get; set; }
    }
}