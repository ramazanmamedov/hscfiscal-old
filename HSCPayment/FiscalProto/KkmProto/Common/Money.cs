using System;
using ProtoBuf;

namespace FiscalProto.KkmProto.Common
{
    [ProtoContract(Name = "Money")]
    [Serializable]
    public class Money
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "bills")]
        public ulong Bills { get; set; }
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "coins")]
        public uint Coins { get; set; }
    }
}