using System;
using ProtoBuf;

namespace FiscalProto.KkmProto.Common
{
    [ProtoContract(Name = "KeyValuePair")]
    [Serializable]
    public class KeyValuePair
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "key")]
        public string Key { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "value")]
        public string Value { get; set; }
    }
}