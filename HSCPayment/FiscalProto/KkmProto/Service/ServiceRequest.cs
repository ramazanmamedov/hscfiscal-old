using System;
using System.Collections.Generic;
using System.ComponentModel;
using FiscalProto.KkmProto.Common;
using ProtoBuf;
using KeyValuePair = FiscalProto.KkmProto.Common.KeyValuePair;

namespace FiscalProto.KkmProto.Service
{
    [Serializable]
    [ProtoContract(Name = "ServiceRequest")]
    public class ServiceRequest
    {
        [DefaultValue(null)]
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "comm_quality")]
        public CommQuality CommQuality { get; set; }

        [DefaultValue(null)]
        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "security_stats")]
        public SecurityStats SecurityStats { get; set; }

        [DefaultValue(null)]
        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "offline_period")]
        public OfflinePeriod OfflinePeriod { get; set; }

        [DefaultValue(null)]
        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "nomenclature_version")]
        public uint NomenclatureVersion { get; set; }

        [ProtoMember(6, DataFormat = DataFormat.Default, Name = "ticket_ad_infos")]
        public List<TicketAdInfo> TicketAdInfos { get; set; } = new List<TicketAdInfo>();

        [DefaultValue(false)]
        [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "get_reg_info")]
        public bool GetRegInfo { get; set; }

        [DefaultValue(false)]
        [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = false, Name = "get_binded_taxation")]
        public bool GetBindedTaxation { get; set; }

        [DefaultValue(0.0f)]
        [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "marker")]
        public ulong Marker { get; set; }

        [DefaultValue(null)]
        [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = false, Name = "software_information")]
        public SoftwareInformation SoftwareInformation { get; set; }

        [DefaultValue(null)]
        [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = false, Name = "reg_info")]
        public RegInfo RegInfo { get; set; }

        [ProtoMember(60, DataFormat = DataFormat.Default, Name = "auxiliary")]
        public List<KeyValuePair> Auxiliary { get; set; } = new List<KeyValuePair>();
    }
}