using System;
using System.Collections.Generic;
using System.ComponentModel;
using KeyValuePair = FiscalProto.KkmProto.Common.KeyValuePair;
using ProtoBuf;

namespace FiscalProto.KkmProto.Service
{
    [ProtoContract(Name = "ModuleInformation")]
    [Serializable]
    public class ModuleInformation
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "name")]
        public string Name { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "version")]
        public string Version { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "build_info")]
        public string BuildInfo { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "device_info")]
        public DeviceInformation DeviceInfo { get; set; }
        
        [ProtoMember(5, DataFormat = DataFormat.Default, Name = "extended_info")]
        public List<KeyValuePair> ExtendedInfo { get; set; } = new List<KeyValuePair>();
        
        [DefaultValue("")]
        [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "build_arch")]
        public string BuildArch { get; set; }
    }
}