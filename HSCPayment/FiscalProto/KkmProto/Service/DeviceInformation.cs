using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.Service
{
    [ProtoContract(Name = "DeviceInformation")]
    [Serializable]
    public class DeviceInformation
    {
        [DefaultValue("")]
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "manufacturer")]
        public string Manufacturer { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "model")]
        public string Model { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "firmware_version")]
        public string FirmwareVersion { get; set; }
    }
}