using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.Service
{
    [ProtoContract(Name = "CommQuality")]
    [Serializable]
    public class CommQuality
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "avg_connect_time")]
        public uint AvgConnectTime { get; set;}
        
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "avg_ping")]
        [DefaultValue(0)]
        public uint AvgPing { get; set; }
        
        [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "loss_ratio")]
        [DefaultValue(0)]
        public uint LossRatio { get; set; }
    }
}