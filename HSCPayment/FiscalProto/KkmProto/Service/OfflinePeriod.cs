using System;
using ProtoBuf;
using DateTime = FiscalProto.KkmProto.DateTimeProto.DateTime;

namespace FiscalProto.KkmProto.Service
{
    [ProtoContract(Name = "OfflinePeriod")]
    [Serializable]
    public class OfflinePeriod
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "begin_time")]
        public DateTime BeginTime { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "end_time")]
        public DateTime EndTime { get; set; }
    }
}