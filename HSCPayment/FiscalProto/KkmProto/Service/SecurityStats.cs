using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.Service
{
    [ProtoContract(Name = "SecurityStats")]
    [Serializable]
    public class SecurityStats
    {
        [DefaultValue(null)]
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "geo_position")]
        public GeoPosition GeoPosition { get; set; }
        
        [DefaultValue(null)]
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "os_version")]
        public string OsVersion { get; set; }
        
    }
}