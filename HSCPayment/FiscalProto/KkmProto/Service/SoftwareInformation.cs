using System.Collections.Generic;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.Service
{
    public class SoftwareInformation
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, Name = "module_infos")]
        public List<ModuleInformation> ModuleInfos { get; set; } = new List<ModuleInformation>();
        
        [DefaultValue("")]
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "hardware_arch")]
        public string HardwareArch { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "os_family")]
        public string OsFamily { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "os_version")]
        public string OsVersion { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "os_extended_info")]
        public string OsExtendedInfo { get; set; }
        
        [DefaultValue("")]
        [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "runtime_version")]
        public string RuntimeVersion { get; set; }
        
        [DefaultValue(0)]
        [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "partner_id")]
        public uint PartnerId { get; set; }
    }
}