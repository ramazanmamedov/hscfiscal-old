using System;
using ProtoBuf;

namespace FiscalProto.KkmProto.Service
{
    [ProtoContract(Name = "GeoPosition")]
    [Serializable]
    public class GeoPosition
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "latitude")]
        public int Latitude { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "longitude")]
        public int Longitude { get; set; }
    }
}