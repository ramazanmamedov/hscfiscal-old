using System;
using System.ComponentModel;
using FiscalProto.KkmProto.RegInfo;
using ProtoBuf;

namespace FiscalProto.KkmProto.Service
{
    [Serializable]
    [ProtoContract(Name = "RegInfo")]
    public class RegInfo
    {
        [DefaultValue(null)]
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "kkm")]   
        public KkmRegInfo Kkm { get; set; }
        
        [DefaultValue(null)]
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "org")]   
        public OrgRegInfo Org { get; set; }
    }
}