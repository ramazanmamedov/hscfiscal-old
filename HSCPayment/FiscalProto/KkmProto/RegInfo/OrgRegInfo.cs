using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.RegInfo
{
    [ProtoContract(Name = "PosRegInfo")]
    [Serializable]
    public class OrgRegInfo
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "title")]
        [DefaultValue("")]
        public string Title { get; set; }
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "address")]
        [DefaultValue("")]
        public string Address { get; set; }
        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "inn")]
        [DefaultValue("")]
        public string Iin { get; set; }
        [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "taxation_type")]
        [DefaultValue(0)]
        public uint TaxationType { get; set; }
        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "okved")]
        [DefaultValue("")]
        public string Okved { get; set; }
    }
}