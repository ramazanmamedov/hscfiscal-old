using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.RegInfo
{
    [ProtoContract(Name = "PosRegInfo")]
    [Serializable]
    public class PosRegInfo
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "title")]
        [DefaultValue("")]
        public string Title { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "address")]
        [DefaultValue("")]
        public string Address { get; set; }
    }
}