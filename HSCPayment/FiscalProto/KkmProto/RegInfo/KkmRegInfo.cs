using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.RegInfo
{
    [ProtoContract(Name = "KkmRegInfo")]
    [Serializable]
    public class KkmRegInfo
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "point_of_payment_number")]
        [DefaultValue("")]
        public string PointPaymentNumber { get; set; }
        
        [DefaultValue("")] 
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "terminal_number")]
        public string TerminalNumber { get; set; }
        
        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "fns_kkm_id")]
        [DefaultValue("")]
        public string FnsKkmId { get; set; }
        
        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "serial_number")]
        [DefaultValue("")]
        public string SerialNumber { get; set; }
        
        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "kkm_id")]
        [DefaultValue("")]
        public string KkmId { get; set; }
    }
}