using System;
using System.ComponentModel;
using FiscalProto.KkmProto.Common;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Amounts")]
    [Serializable]
    public class Amounts
    {
        [ProtoMember(1, Name = "total", IsRequired = true, DataFormat = DataFormat.Default)]
        public Money Total { get; set; }

        [ProtoMember(2, Name = "taken", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(null)]
        public Money Taken { get; set; }

        [ProtoMember(3, Name = "change", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(null)]
        public Money Change { get; set; }

        [ProtoMember(4, Name = "markup", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(null)]
        public Modifier Markup { get; set; }

        [ProtoMember(5, Name = "discount", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(null)]
        public Modifier Discount { get; set; }
    }
}