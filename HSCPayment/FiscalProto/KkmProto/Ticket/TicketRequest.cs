using System;
using System.Collections.Generic;
using System.ComponentModel;
using FiscalProto.KkmProto.Common;
using FiscalProto.KkmProto.Enums;
using ProtoBuf;
using DateTime = FiscalProto.KkmProto.DateTimeProto.DateTime;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "TicketRequest")]
    [Serializable]
    public class TicketRequest
    {
        [ProtoMember(1, Name = "operation", IsRequired = true, DataFormat = DataFormat.TwosComplement)]
        public OperationTypeEnum Operation { get; set; }

        [ProtoMember(2, Name = "date_time", IsRequired = true, DataFormat = DataFormat.TwosComplement)]
        public DateTime DateTime { get; set; }

        [ProtoMember(3, Name = "operator", IsRequired = true, DataFormat = DataFormat.Default)]
        public Operator Operator { get; set; }

        [ProtoMember(4, Name = "domain", IsRequired = true, DataFormat = DataFormat.Default)]
        public Domain Domain { get; set; }

        [ProtoMember(5, Name = "items", DataFormat = DataFormat.Default)]
        public List<Item> Items { get; set; }

        [ProtoMember(6, Name = "payments", DataFormat = DataFormat.Default)]
        public List<Payment> Payments { get; set; }
        
        [ProtoMember(7, Name = "taxes", DataFormat = DataFormat.Default)]
        public List<Tax> Taxes { get; set; }

        [ProtoMember(8, Name = "amounts", IsRequired = true, DataFormat = DataFormat.Default)]
        public Amounts Amounts { get; set; }

        [ProtoMember(9, Name = "extension_options", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(null)]
        public ExtensionOptions ExtensionOptions { get; set; }

        [ProtoMember(10, Name = "offline_ticket_number", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(0)]
        public uint OfflineTicketNumber { get; set; }

        [ProtoMember(11, Name = "printed_document_number", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue("")]
        public string PrintDocumentNumber { get; set; }

        [ProtoMember(12, Name = "printed_ticket", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue("")]
        public string PrintedTicket { get; set; }

        [ProtoMember(13, Name = "ad_infos", DataFormat = DataFormat.Default)]
        public List<TicketAdInfo> adInfos { get; set; }

        [ProtoMember(14, Name = "fr_shift_number", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(0)]
        public uint FrShiftNumber { get; set; }

        [ProtoMember(15, Name = "shift_document_number", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(0)]
        public uint ShiftDocumentNumber { get; set; }

    }
}