using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "TicketResponse")]
    [Serializable]
    
    public class TicketResponse
    {
        [ProtoMember(1, Name = "ticket_number", IsRequired = true, DataFormat = DataFormat.Default)]
        public string TicketNumber { get; set; }

        [ProtoMember(2, Name = "qr_code", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue(null)]
        public byte[] QrCode { get; set; }
    }
}