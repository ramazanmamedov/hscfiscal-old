using System;
using FiscalProto.KkmProto.Common;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Taxi")]
    [Serializable]
    public class Taxi
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "car_number")]
        public string CarNumber { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "is_order")]
        public bool IsOrder { get; set; }

        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "current_fee")]
        public Money CurrentFee { get; set; }
    }
}