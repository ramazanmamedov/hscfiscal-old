using System;
using FiscalProto.KkmProto.Common;
using FiscalProto.KkmProto.Enums;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Payment")]
    [Serializable]
    public class Payment
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "type")]
        public PaymentTypeEnum Type { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "sum")]
        public Money Sum { get; set; }
    }
}