using System;
using System.Collections.Generic;
using System.ComponentModel;
using FiscalProto.KkmProto.Common;
using ProtoBuf;
using KeyValuePair = FiscalProto.KkmProto.Common.KeyValuePair;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Modifier")]
    [Serializable]
    public class Modifier
    {
        [ProtoMember(1, Name = "name", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue("")]
        public string Name { get; set; }
        
        [ProtoMember(2, Name = "sum", IsRequired = true, DataFormat = DataFormat.TwosComplement)]
        public Money Sum { get; set; }
        
        [ProtoMember(3, Name = "taxes", DataFormat = DataFormat.Default)]
        public List<Tax> Taxes { get; set; }
        
        [ProtoMember(4, Name = "auxiliary", DataFormat = DataFormat.Default)]
        public List<KeyValuePair> Auxiliary { get; set; }
    }
}