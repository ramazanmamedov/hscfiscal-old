using System;
using ProtoBuf;
using DateTime = FiscalProto.KkmProto.DateTimeProto.DateTime;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Parking")]
    [Serializable]
    public class Parking
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "begin_time")]
        public DateTime BeginTime { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "end_time")]
        public DateTime EndTime { get; set; }
    }
}