using System;
using System.Collections.Generic;
using System.ComponentModel;
using ProtoBuf;
using KeyValuePair = FiscalProto.KkmProto.Common.KeyValuePair;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "ExtensionOptions")]
    [Serializable]
    public class ExtensionOptions
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "customer_email")]
        [DefaultValue("")]
        public string CustomerEmail { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "customer_phone")]
        [DefaultValue("")]
        public string CustomerPhone { get; set; }

        [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "auxiliary")]
        public List<KeyValuePair> Auxiliary { get; set; }
    }
}