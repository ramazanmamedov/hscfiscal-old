using System;
using System.ComponentModel;
using FiscalProto.KkmProto.Enums;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Item")]
    [Serializable]
    public class Item
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "type")]
        public ItemTypeEnum Type { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "commodity")]
        [DefaultValue(null)]
        public Commodity Commodity { get; set; }

        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "storno_commodity")]
        [DefaultValue(null)]
        public StornoCommodity StornoCommodity { get; set; }

        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "markup")]
        [DefaultValue(null)]
        public Modifier Markup { get; set; }

        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "storno_markup")]
        [DefaultValue(null)]
        public Modifier StornoMarkup { get; set; }

        [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "discount")]
        [DefaultValue(null)]
        public Modifier Discount { get; set; }

        [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "storno_discount")]
        [DefaultValue(null)]
        public Modifier StornoDiscount { get; set; }
    }
}