using System;
using FiscalProto.KkmProto.Common;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Tax")]
    [Serializable]
    public class Tax
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "tax_type")]
        public uint TaxType { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "taxation_type")]
        public uint TaxationType { get; set; }
        
        [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "percent")]
        public uint Percent { get; set; }
        
        [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "sum")]
        public Money Sum { get; set; }
        
        [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "is_in_total_sum")]
        public bool IsInTotalSum { get; set; }
    }
}