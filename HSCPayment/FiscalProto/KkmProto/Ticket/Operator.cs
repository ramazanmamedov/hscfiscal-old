using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Operator")]
    [Serializable]
    public class Operator
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "code")]
        public uint Code { get; set; }
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "name")]
        [DefaultValue("")]
        public string Name { get; set; }
    }
}