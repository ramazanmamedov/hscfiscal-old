using System;
using System.ComponentModel;
using FiscalProto.KkmProto.Enums;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Domain")]
    [Serializable]
    public class Domain
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "type")]
        public DomainTypeEnum Type { get; set; }

        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "services")]
        [DefaultValue(null)]
        public Services Services { get; set; }

        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "gasoil")]
        [DefaultValue(null)]
        public GasOil GasOil { get; set; }

        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "taxi")]
        [DefaultValue(null)]
        public Taxi Taxi { get; set; }

        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "parking")]
        [DefaultValue(null)]
        public Parking Parking { get; set; }
    }
}