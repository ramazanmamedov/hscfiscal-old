using System;
using System.ComponentModel;
using FiscalProto.KkmProto.Common;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "GasOil")]
    [Serializable]
    public class GasOil
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "correction_number")]
        [DefaultValue("")]
        public string CorrectionNumber { get; set; }

        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "correction_sum")]
        [DefaultValue("")]
        public Money CorrectionSum { get; set; }

        [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "card_number")]
        [DefaultValue("")]
        public string CardNumber { get; set; }
    }
}