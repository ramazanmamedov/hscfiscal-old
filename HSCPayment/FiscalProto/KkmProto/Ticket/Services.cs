using System;
using ProtoBuf;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Services")]
    [Serializable]
    public class Services
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "account_number")]
        public string AccountNumber { get; set; }
    }
}