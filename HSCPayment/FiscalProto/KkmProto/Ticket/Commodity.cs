using System;
using System.Collections.Generic;
using System.ComponentModel;
using FiscalProto.KkmProto.Common;
using ProtoBuf;
using KeyValuePair = FiscalProto.KkmProto.Common.KeyValuePair;

namespace FiscalProto.KkmProto.Ticket
{
    [ProtoContract(Name = "Commodity")]
    [Serializable]
    public class Commodity
    {
        [ProtoMember(1, Name = "code", IsRequired = false, DataFormat = DataFormat.TwosComplement)]
        [DefaultValue(0)]
        public uint Code { get; set; }
        
        [ProtoMember(2, Name = "name", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue("")]
        public string Name { get; set; }
        
        [ProtoMember(3, Name = "section_code", IsRequired = true, DataFormat = DataFormat.Default)]
        public string SectionCode { get; set; }
        
        [ProtoMember(4, Name = "quantity", IsRequired = true, DataFormat = DataFormat.TwosComplement)]
        public uint Quantity { get; set; }
        
        [ProtoMember(5, Name = "price", IsRequired = true, DataFormat = DataFormat.TwosComplement)]
        public Money Price { get; set; }
        
        [ProtoMember(6, Name = "sum", IsRequired = true, DataFormat = DataFormat.TwosComplement)]
        public Money Sum { get; set; }
        
        [ProtoMember(7, Name = "taxes", DataFormat = DataFormat.Default)]
        public List<Tax> Taxes { get; set; }
        
        [ProtoMember(8, Name = "excise_stamp", IsRequired = false, DataFormat = DataFormat.Default)]
        [DefaultValue("")]
        public string ExciseStamp { get; set; }
        
        [ProtoMember(9, Name = "auxiliary", DataFormat = DataFormat.Default)]
        public List<KeyValuePair> Auxiliary { get; set; }
    }
}