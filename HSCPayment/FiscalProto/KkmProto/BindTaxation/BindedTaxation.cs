using System;
using System.Collections.Generic;
using ProtoBuf;

namespace FiscalProto.KkmProto.BindTaxation
{
    [ProtoContract(Name = "BindedTaxation")]
    [Serializable]
    public class BindedTaxation
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "taxation_type")]
        public uint TaxationType { get; set; }
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "taxes")]
        public List<BindedTax> Taxes { get; set; }
    }
}