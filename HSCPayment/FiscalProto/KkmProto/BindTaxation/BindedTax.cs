using System;
using ProtoBuf;

namespace FiscalProto.KkmProto.BindTaxation
{
    [ProtoContract(Name = "BindedTax")]
    [Serializable]
    public class BindedTax
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "tax_type")]
        public uint TaxType { get; set; }
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "percent")]
        public uint Percent { get; set; }
    }
}