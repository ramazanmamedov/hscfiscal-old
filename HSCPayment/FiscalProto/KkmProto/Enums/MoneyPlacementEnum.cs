using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "ItemTypeEnum")]
    public enum MoneyPlacementEnum
    {
        [ProtoEnum(Name = "MONEY_PLACEMENT_DEPOSIT", Value = 0)]
        MONEY_PLACEMENT_DEPOSIT = 0,
        [ProtoEnum(Name = "MONEY_PLACEMENT_WITHDRAWAL", Value = 1)]
        MONEY_PLACEMENT_WITHDRAWAL = 1
    }
}