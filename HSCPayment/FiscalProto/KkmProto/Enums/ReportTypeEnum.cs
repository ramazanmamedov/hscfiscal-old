using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "PaymentTypeEnum")]
    public enum ReportTypeEnum
    {
        [ProtoEnum(Name = "REPORT_Z", Value = 0)]
        REPORT_Z = 0,
        [ProtoEnum(Name = "REPORT_X", Value = 1)]
        REPORT_X = 1
    }
}