using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "DomainTypeEnum")]
    public enum DomainTypeEnum
    {
        [ProtoEnum(Name = "DOMAIN_TRADING", Value = 0)] DOMAIN_TRADING = 0,
        [ProtoEnum(Name = "DOMAIN_SERVICES", Value = 1)] DOMAIN_SERVICES = 1,
        [ProtoEnum(Name = "DOMAIN_GASOIL", Value = 2)] DOMAIN_GASOIL = 2,
        [ProtoEnum(Name = "DOMAIN_HOTELS", Value = 3)] DOMAIN_HOTELS = 3,
        [ProtoEnum(Name = "DOMAIN_TAXI", Value = 4)] DOMAIN_TAXI = 4,
        [ProtoEnum(Name = "DOMAIN_PARKING", Value = 5)] DOMAIN_PARKING = 5
    }
}