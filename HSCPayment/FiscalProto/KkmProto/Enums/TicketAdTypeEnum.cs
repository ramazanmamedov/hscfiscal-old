using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "TicketAdTypeEnum")]
    public enum TicketAdTypeEnum
    {
        [ProtoEnum(Name = "TICKET_AD_OFD", Value = 0)] TICKET_AD_OFD = 0,
        [ProtoEnum(Name = "TICKET_AD_ORG", Value = 1)] TICKET_AD_ORG = 1,
        [ProtoEnum(Name = "TICKET_AD_POS", Value = 2)] TICKET_AD_POS = 2,
        [ProtoEnum(Name = "TICKET_AD_KKM", Value = 3)] TICKET_AD_KKM = 3,
        [ProtoEnum(Name = "TICKET_AD_INFO", Value = 4)] TICKET_AD_INFO = 4
    }
}