using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "ItemTypeEnum")]
    public enum ItemTypeEnum
    {
        [ProtoEnum(Name = "ITEM_TYPE_COMMODITY", Value = 1)] ITEM_TYPE_COMMODITY = 1,
        [ProtoEnum(Name = "ITEM_TYPE_STORNO_COMMODITY", Value = 2)] ITEM_TYPE_STORNO_COMMODITY = 2,
        [ProtoEnum(Name = "ITEM_TYPE_MARKUP", Value = 3)] ITEM_TYPE_MARKUP = 3,
        [ProtoEnum(Name = "ITEM_TYPE_STORNO_MARKUP", Value = 4)] ITEM_TYPE_STORNO_MARKUP = 4,
        [ProtoEnum(Name = "ITEM_TYPE_DISCOUNT", Value = 5)] ITEM_TYPE_DISCOUNT = 5,
        [ProtoEnum(Name = "ITEM_TYPE_STORNO_DISCOUNT", Value = 6)] ITEM_TYPE_STORNO_DISCOUNT = 6
    }
}