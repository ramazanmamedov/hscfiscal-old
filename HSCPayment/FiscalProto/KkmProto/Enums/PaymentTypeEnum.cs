using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "PaymentTypeEnum")]
    public enum PaymentTypeEnum
    {
        [ProtoEnum(Name = "PAYMENT_CASH", Value = 0)] PAYMENT_CASH = 0,
        [ProtoEnum(Name = "PAYMENT_CARD", Value = 1)] PAYMENT_CARD = 1,
        [ProtoEnum(Name = "PAYMENT_CREDIT", Value = 2)] PAYMENT_CREDIT = 2,
        [ProtoEnum(Name = "PAYMENT_TARE", Value = 3)] PAYMENT_TARE = 3
    }
}