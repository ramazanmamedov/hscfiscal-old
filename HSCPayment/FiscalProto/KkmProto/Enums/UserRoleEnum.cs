using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "UserRoleEnum")]
    public enum UserRoleEnum
    {
        [ProtoEnum(Name = "USER_ROLE_PAYMASTER", Value = 1)]
        USER_ROLE_PAYMASTER = 1,
        [ProtoEnum(Name = "USER_ROLE_CHIEF_PAYMASTER", Value = 2)]
        USER_ROLE_CHIEF_PAYMASTER = 2,
        [ProtoEnum(Name = "USER_ROLE_ADMINISTRATOR", Value = 3)]
        USER_ROLE_ADMINISTRATOR = 3
    }
}