using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "CommandTypeEnum")]
    public enum CommandTypeEnum
    {
        [ProtoEnum(Name = "COMMAND_SYSTEM", Value = 0)]
        COMMAND_SYSTEM = 0,
        [ProtoEnum(Name = "COMMAND_TICKET", Value = 1)]
        COMMAND_TICKET = 1,
        [ProtoEnum(Name = "COMMAND_CLOSE_SHIFT", Value = 2)]
        COMMAND_CLOSE_SHIFT = 2,
        [ProtoEnum(Name = "COMMAND_REPORT", Value = 3)]
        COMMAND_REPORT = 3,
        [ProtoEnum(Name = "COMMAND_NOMENCLATURE", Value = 4)]
        COMMAND_NOMENCLATURE = 4,
        [ProtoEnum(Name = "COMMAND_INFO", Value = 5)]
        COMMAND_INFO = 5,
        [ProtoEnum(Name = "COMMAND_MONEY_PLACEMENT", Value = 6)]
        COMMAND_MONEY_PLACEMENT = 6,
        [ProtoEnum(Name = "COMMAND_CANCEL_TICKET", Value = 7)]
        COMMAND_CANCEL_TICKET = 7,
        [ProtoEnum(Name = "COMMAND_AUTH", Value = 8)]
        COMMAND_AUTH = 8,
        [ProtoEnum(Name = "COMMAND_RESERVED", Value = 127)]
        COMMAND_RESERVED = 127
    }
}