using ProtoBuf;

namespace FiscalProto.KkmProto.Enums
{
    [ProtoContract(Name = "OperationTypeEnum")]
    public enum OperationTypeEnum
    {
        [ProtoEnum(Name = "OPERATION_BUY", Value = 0)]
        OPERATION_BUY = 0,

        [ProtoEnum(Name = "OPERATION_BUY_RETURN", Value = 1)]
        OPERATION_BUY_RETURN = 1,

        [ProtoEnum(Name = "OPERATION_SELL", Value = 2)]
        OPERATION_SELL = 2,

        [ProtoEnum(Name = "OPERATION_SELL_RETURN", Value = 3)]
        OPERATION_SELL_RETURN = 3
    }
}