using System;
using System.ComponentModel;
using ProtoBuf;

namespace FiscalProto.KkmProto.DateTimeProto
{
    [ProtoContract(Name = "Time")]
    [Serializable]
    public class Time
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "hour")]
        public uint Hour { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "minute")]
        public uint Minute { get; set; }
        
        [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "second")]
        [DefaultValue(0)]
        public uint Second { get; set; }
    }
}