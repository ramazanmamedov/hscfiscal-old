using System;
using ProtoBuf;

namespace FiscalProto.KkmProto.DateTimeProto
{
    [ProtoContract(Name = "DateTime")]
    [Serializable]
    public class DateTime
    {
        [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "date")]
        public Date Date { get; set; }
        
        [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "time")]
        public Time Time { get; set; }
    }
}