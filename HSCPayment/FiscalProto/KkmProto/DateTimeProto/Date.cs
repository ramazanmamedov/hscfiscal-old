using System;
using ProtoBuf;

namespace FiscalProto.KkmProto.DateTimeProto
{
    [ProtoContract(Name = "Date")]
    [Serializable]
    public class Date
    {
        [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "year")]
        public uint Year { get; set; }
        [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "month")]
        public uint Month { get; set; }
        [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "day")]
        public uint Day { get; set; }
    }
}