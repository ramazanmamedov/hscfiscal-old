using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Models.Services
{
    public interface IHttpService
    {
        [HttpPost]
        Task<dynamic> Post([FromBody] object anyObject);
    }
}