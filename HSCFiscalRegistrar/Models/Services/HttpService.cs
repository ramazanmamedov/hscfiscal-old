using System.Threading.Tasks;
using Flurl.Http;
using Microsoft.AspNetCore.Mvc;

namespace Models.Services
{
    public class HttpService : IHttpService
    {
        private const string Url = "http://52.38.152.232:8082";

        [HttpPost]
        public async Task<dynamic> Post([FromBody] object anyObject)
        {
            dynamic x = await Url.PostJsonAsync(anyObject).ReceiveJson();
            return x;
        }
    }
}
