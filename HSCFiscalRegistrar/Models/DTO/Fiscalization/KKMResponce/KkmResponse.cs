﻿namespace Models.DTO.Fiscalization.KKMResponce
{
    public class KkmResponse
    {
        public Data Data { get; set; }

        public KkmResponse()
        {
        }

        public KkmResponse(Operation operation, Shift shift)
        {
            Data = new Data
            {
                DateTime = operation.CreationDate.ToString("g"),
                CheckNumber = operation.FiscalNumber,
                OfflineMode = true,
                Cashbox = new Cashbox
                {
                    Address = operation.Kkm.Address,
                    IdentityNumber = operation.Kkm.DeviceId.ToString(),
                    UniqueNumber = operation.Kkm.SerialNumber,
                    RegistrationNumber = operation.Kkm.FnsKkmId
                },
                CashboxOfflineMode = false,
                CheckOrderNumber = operation.Kkm.ReqNum,
                ShiftNumber = shift.Number,
                EmployeeName = operation.User.UserName,
                TicketUrl = operation.Qr,
            };
        }
    }
}