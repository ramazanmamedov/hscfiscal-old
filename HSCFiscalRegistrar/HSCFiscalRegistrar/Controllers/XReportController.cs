﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HSCFiscalRegistrar.Helpers;
using HSCFiscalRegistrar.OfdRequests;
using HSCFiscalRegistrar.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.DTO.XReport;
using Models.DTO.XReport.KkmResponse;
using Models.Services;
using Newtonsoft.Json;
using DateTime = System.DateTime;

namespace HSCFiscalRegistrar.Controllers
{
    [Route("api/[controller]")]
    public class XReportController : Controller
    {
        private readonly ApplicationContext _applicationContext;
        private readonly UserManager<User> _userManager;
        private readonly GenerateErrorHelper _errorHelper;
        private readonly IHttpService _httpService;

        public XReportController(ApplicationContext applicationContext, UserManager<User> userManager, 
            GenerateErrorHelper errorHelper, IHttpService httpService)
        {
            _applicationContext = applicationContext;
            _userManager = userManager;
            _errorHelper = errorHelper;
            _httpService = httpService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] KkmRequest request)
        {
            try
            {
                User user = _userManager.Users.FirstOrDefault(u => u.UserToken == request.Token);
                Kkm kkm = _applicationContext.Kkms.FirstOrDefault(k => k.Id == user.KkmId);
                if (kkm == null) return Json( _errorHelper.GetErrorRequest(3));
                User merch = _userManager.Users.FirstOrDefault(u => user.OwnerId == u.Id);
                Shift shift;
                try
                {
                    shift = _applicationContext.Shifts.Last(s => s.KkmId == kkm.Id && s.CloseDate == DateTime.MinValue);
                }
                catch (Exception)
                {
                    shift = await GetShift(user,kkm);
                }
                IQueryable<Operation> operations = _applicationContext.Operations.Where(o => o.ShiftId == shift.Id);
                List<ShiftOperation> shiftOperations = ZxReportService.GetShiftOperations(operations, shift);
                ZxReportService.AddShiftProps(shift, operations);
                XReportKkmResponse response = new XReportKkmResponse(shiftOperations, operations, merch, kkm, shift);
                kkm.ReqNum += 1;
                await _applicationContext.ShiftOperations.AddRangeAsync(shiftOperations);
                await _applicationContext.SaveChangesAsync();
                OfdXReport xReportOfdRequest = new OfdXReport(_httpService);
                xReportOfdRequest.Request(kkm, merch);

                return Ok(JsonConvert.SerializeObject(response));
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        
        private async Task<Shift> GetShift(User oper, Kkm kkm)
        {
            Shift shift;
            if (!_applicationContext.Shifts.Any())
            {
                shift = new Shift
                {
                    OpenDate = DateTime.Now,
                    KkmId = kkm.Id,
                    Number = 1,
                    UserId = oper.Id
                };
                await _applicationContext.Shifts.AddAsync(shift);
                await _applicationContext.SaveChangesAsync();
            }
            else if (_applicationContext.Shifts.Last().CloseDate != DateTime.MinValue)
            {
                shift = new Shift
                {
                    OpenDate = DateTime.Now,
                    KkmId = kkm.Id,
                    UserId =  oper.Id,
                    Number = _applicationContext.Shifts.Last().Number + 1,
                    BuySaldoBegin = _applicationContext.Shifts.Last().BuySaldoEnd,
                    SellSaldoBegin = _applicationContext.Shifts.Last().SellSaldoEnd,
                    RetunBuySaldoBegin = _applicationContext.Shifts.Last().RetunBuySaldoEnd,
                    RetunSellSaldoBegin = _applicationContext.Shifts.Last().RetunSellSaldoEnd,
                };
                await _applicationContext.Shifts.AddAsync(shift);
                await _applicationContext.SaveChangesAsync();
            }

            shift = _applicationContext.Shifts.Last(s =>s.UserId == oper.Id);
            return shift;
        }

    }
}