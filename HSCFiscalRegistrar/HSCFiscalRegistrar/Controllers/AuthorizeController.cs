﻿using System;
using System.Linq;
using System.Threading.Tasks;
using HSCFiscalRegistrar.Exceptions;
using HSCFiscalRegistrar.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Models;
using Models.DTO.Auth;
using Models.DTO.UserModel;
using Serilog;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace HSCFiscalRegistrar.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizeController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly GenerateUserToken _generateUserToken;

        public AuthorizeController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            GenerateUserToken generateUserToken)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _generateUserToken = generateUserToken;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDTO model)
        {
            try
            {
                Log.Information("Authorize|Post");
                Log.Information($"Авторизация пользователя: {model}");

                SignInResult result = await _signInManager.PasswordSignInAsync(model.Login,
                    model.Password,
                    false,
                    false).ConfigureAwait(false);

                if (result.Succeeded)
                {
                    User appUser = _userManager.Users.SingleOrDefault(r => r.Email == model.Login);
                    if (appUser != null)
                    {
                        appUser.DateTimeCreationToken = _generateUserToken.TimeCreation();
                        appUser.ExpiryDate = _generateUserToken.ExpiryDate();
                        appUser.UserToken = _generateUserToken.Token(appUser.Id);
                        IdentityResult response = await _userManager.UpdateAsync(appUser);

                        if (!response.Succeeded) throw new DbUpdateException("Ошибка обновления дб");
                        AnswerServerAuth dto = new AnswerServerAuth
                        {
                            Data = new Data
                            {
                                Token = appUser.UserToken
                            }
                        };
                        return Ok(JsonConvert.SerializeObject(dto));
                    }
                }

                Log.Error($"Ошибка авторизации пользователя: {model.Login}");
                throw new AuthorizeException("Неверный логин или пароль");
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return Ok(e.Message);
            }
        }
    }
}