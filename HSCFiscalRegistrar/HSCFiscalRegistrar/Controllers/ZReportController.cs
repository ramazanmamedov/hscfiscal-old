using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HSCFiscalRegistrar.Helpers;
using HSCFiscalRegistrar.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.DTO.CloseShift;
using Models.DTO.CloseShift.OfdResponse;
using Models.DTO.XReport;
using Models.DTO.XReport.KkmResponse;
using Models.Services;
using Newtonsoft.Json;
using Serilog;

namespace HSCFiscalRegistrar.Controllers
{
    [Route("api/[controller]")]
    public class ZReportController : Controller
    {
        private readonly ApplicationContext _applicationContext;
        private readonly UserManager<User> _userManager;
        private readonly GenerateErrorHelper _errorHelper;
        private readonly IHttpService _httpService;

        public ZReportController(ApplicationContext applicationContext, UserManager<User> userManager, 
            GenerateErrorHelper errorHelper, IHttpService httpService)
        {
            _applicationContext = applicationContext;
            _userManager = userManager;
            _errorHelper = errorHelper;
            _httpService = httpService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] KkmRequest request)
        {
            try
            {
                Log.Information("ZReport|Post");
                Log.Information($"Z-Отчет: {request.Token}");
                
                User user = _userManager.Users.FirstOrDefault(u => u.UserToken == request.Token);
                Kkm kkm = _applicationContext.Kkms.FirstOrDefault(k => k.UserId == user.Id);
                Shift shift = _applicationContext.Shifts.Last(s => s.KkmId == kkm.Id);
                IQueryable<Operation> operations = _applicationContext.Operations.Where(o => o.ShiftId == shift.Id);
                List<ShiftOperation> shiftOperations = ZxReportService.GetShiftOperations(operations, shift);
                ZxReportService.AddShiftProps(shift, operations);
                ZxReportService.CloseShift(true, shift);
                User merch = _userManager.Users.FirstOrDefault(u => u.Id == kkm.UserId);
                Task<CloseShiftOfdResponse> closeShiftOfdResponse = OfdRequest(kkm, merch, shift.Number);
                if (kkm == null) return Json(_errorHelper.GetErrorRequest(3));
                kkm.OfdToken = closeShiftOfdResponse.Result.Token;
                kkm.ReqNum += 1;
                XReportKkmResponse response = new XReportKkmResponse(shiftOperations, operations, user, kkm, shift);
                _applicationContext.ShiftOperations.AddRangeAsync(shiftOperations);
                _applicationContext.SaveChangesAsync();
                return Ok(JsonConvert.SerializeObject(response));
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return Json(e.Message);
            }
        }

        private async Task<CloseShiftOfdResponse> OfdRequest(Kkm kkm, User org, int shiftNumber)
        {
            
            CloseShiftRequest closeShiftRequest = new CloseShiftRequest(kkm, org, shiftNumber);
            try
            {
                Log.Information("OfdCloseShiftRequest|Post");
                Log.Information("Отправка запроса на закрытие смены в ОФД");
                await _httpService.Post(closeShiftRequest);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
            }

            dynamic x = await _httpService.Post(closeShiftRequest);
            CloseShiftOfdResponse response = JsonConvert.DeserializeObject<CloseShiftOfdResponse>(x);
            return response;
        }
    }
}