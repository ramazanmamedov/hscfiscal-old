using System;
using Models;
using Models.DTO.XReport.OfdRequest;
using Models.Services;
using Serilog;

namespace HSCFiscalRegistrar.OfdRequests
{
    public class OfdXReport
    {
        private readonly IHttpService _httpService;

        public OfdXReport(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public async void Request(Kkm kkm, User user)
        {
            
            XReportOfdRequestModel request = new XReportOfdRequestModel(kkm, user);
            try
            {
                Log.Information("OfdXReport|Post");
                Log.Information("Отправка запроса на Х-Отчет в ОФД");
                await _httpService.Post(request);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
            }
            
        }
    }
}