﻿using System;
using System.Text.RegularExpressions;

namespace HSCFiscalRegistrar.Helpers
{
    public class GenerateUserToken
    {
        private const int Hours = 24;
        public string GetGuidKey()
        {
            return Regex.Replace(
                Convert.ToBase64String(Guid.NewGuid().ToByteArray()), 
                "[/+=]", 
                "");
        }

        public DateTime TimeCreation()
        {
            return DateTime.Now;
        }

        public DateTime ExpiryDate()
        {
            TimeSpan time = new TimeSpan(0, Hours, 0, 0);
            DateTime combined = DateTime.Now.Add(time);
            return combined;
        }

        public string Token(string appUserId)
        {
            return $"{appUserId}%{GetGuidKey()}";
        }
    }
}