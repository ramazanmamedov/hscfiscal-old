using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HSCFiscalRegistrar;
using HSCFiscalRegistrar.Controllers;
using HSCFiscalRegistrar.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.DTO.CloseShift.OfdResponse;
using Models.DTO.XReport;
using Models.Enums;
using Models.Services;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using TestProject1.FakeTestModels.Tests;

namespace TestProject1.TestControllers
{
    [TestFixture]
    public class TestZReportController
    {
        private ZReportController _controller;
        private Mock<FakeUserManager> _userManagerMock;
        private ApplicationContext _context;
        private Mock<GenerateErrorHelper> _errorHelper;
        private DbContextOptions<ApplicationContext> _options;
        private Mock<IHttpService> _httpService;
        
        [SetUp]
        public void Setup()
        {
            _httpService = new Mock<IHttpService>();
            _userManagerMock = new Mock<FakeUserManager>();
            _errorHelper = new Mock<GenerateErrorHelper>();
            _options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase("TestZReport")
                .Options;
            _httpService.Setup(h => h.Post(It.IsAny<object>())).Returns(GetResponseOfd());
            _context = new ApplicationContext(_options);
            _userManagerMock.Setup(r => r.Users).Returns(GetUsers());
            _controller = new ZReportController(_context, _userManagerMock.Object, _errorHelper.Object, _httpService.Object);
            _userManagerMock.Setup(u => u.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(GetUser());
            _context.Kkms.Add(GetKkm());
            _context.Shifts.Add(GetLastShift());
            _context.Operations.AddRangeAsync(GetOperation());
            _context.SaveChangesAsync();
        }
        
        private async Task<dynamic> GetResponseOfd()
        {
            string path = @"TestControllers/Files/ZXReport.json";
            string json = await File.ReadAllTextAsync(path);
            var ofdResp = JsonConvert.DeserializeObject<CloseShiftOfdResponse>(json);
            string result = JsonConvert.SerializeObject(ofdResp);
            return new JsonResult(result).Value;
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void TestPostMethod()
        {
            var result = (OkObjectResult)_controller.Post(KkmRequest());
            CloseShiftOfdResponse answer = JsonConvert.DeserializeObject<CloseShiftOfdResponse>(result.Value.ToString());
            
            Assert.IsNotNull(answer);
        }

        private KkmRequest KkmRequest()
        {
            return new KkmRequest
            {
                Token = "sd",
                CashboxUniqueNumber = "1234567"
            };
        }

        private List<Operation> GetOperation()
        {
            return new List<Operation>
            {
                new Operation
                {
                    Amount = 125,
                    Id = "1",
                    Type = OperationTypeEnum.OPERATION_BUY,
                    CashAmount = 15,
                    KkmId = "1",
                    UserId = "1",
                    ShiftId = "1",
                    FiscalNumber = "123456",
                    CheckNumber = 1,
                    CreationDate = DateTime.Now
                },
                new Operation
                {
                    Amount = 125,
                    Id = "2",
                    Type = OperationTypeEnum.OPERATION_BUY,
                    CashAmount = 15,
                    KkmId = "1",
                    UserId = "1",
                    ShiftId = "1",
                    FiscalNumber = "123456",
                    CheckNumber = 1,
                    CreationDate = DateTime.Now
                },
                new Operation
                {
                    Amount = 125,
                    Id = "3",
                    Type = OperationTypeEnum.OPERATION_BUY,
                    CashAmount = 15,
                    KkmId = "1",
                    UserId = "1",
                    ShiftId = "1",
                    FiscalNumber = "123456",
                    CheckNumber = 1,
                    CreationDate = DateTime.Now
                }
            };
        }

        private IQueryable<User> GetUsers()
        {
            List<User> users = new List<User>
            {
                new User
                {
                    Email = "admin@gmail.com",
                    Id = Guid.NewGuid().ToString()
                },
                new User
                {
                    Email = "nurs@gmail.com",
                    Id = Guid.NewGuid().ToString()
                },
                new User
                {
                    Email = "ibrahim@gmail.com",
                    Id = Guid.NewGuid().ToString()
                }
            };
            users.Add(GetUser());
            return users.AsQueryable();
        }
        
        private User GetUser()
        {
            return new User
            {
                Id = "1",
                UserName = "admin",
                Address = "Abay",
                Email = "admin@gmail.com",
                Fio = "Ibrahim",
                Inn = "777",
                Okved = "0",
                Title = "Title",
                KkmId = "1",
                VAT = true,
                VATSeria = "123456",
                VATNumber = "1235678",
                OwnerId = "1",
                OperatorCode = 1,
                TaxationType = TaxationTypeEnum.RTS,
                UserToken = "sd"
            };
        }

        private Shift GetLastShift()
        {
            return new Shift
            {
                Id = "1",
                UserId = "1",
                KkmId = "1",
                OpenDate = DateTime.Now.AddDays(-1),
                CloseDate = DateTime.MinValue,
                Number = 1,
                BuySaldoBegin = 15,
                BuySaldoEnd = 25,
                KkmBalance = 250,
                SellSaldoBegin = 15,
                SellSaldoEnd = 25
            };
        }
        
        private Kkm GetKkm()
        {
            return new Kkm
            {
                Address = "Abay",
                Description = "New",
                Id = "1",
                Iin = "7777777777",
                Mode = "online",
                Name = "New",
                User = new User(),
                CurrentStatus = "online",
                DeviceId = 2731,
                IdentificationNumber = "128838383",
                NameOrg = "FinTeh",
                OfdToken = 9999999,
                OperatorId = "1",
                ReqNum = 123,
                SerialNumber = "12345",
                TerminalNumber = "fsdfsf",
                UserId = "1",
                FnsKkmId = "12345",
                LastDateOperation = DateTime.Now,
                PointOfPayment = "1234"
            };
        }
    }
}