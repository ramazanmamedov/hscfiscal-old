using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HSCFiscalRegistrar;
using HSCFiscalRegistrar.Controllers;
using HSCFiscalRegistrar.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.DTO.CloseShift.OfdResponse;
using Models.DTO.XReport;
using Models.Enums;
using Models.Services;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using TestProject1.FakeTestModels.Tests;

namespace TestProject1.TestControllers
{
    [TestFixture]
    public class TestXReportController
    {
        private XReportController _controller;
        private Mock<FakeUserManager> _userManagerMock;
        private ApplicationContext _context;
        private Mock<GenerateErrorHelper> _errorHelper;
        private DbContextOptions<ApplicationContext> _options;
        private Mock<IHttpService> _httpService;

        [SetUp]
        public void Setup()
        {
            _httpService = new Mock<IHttpService>();
            _userManagerMock = new Mock<FakeUserManager>();
            _errorHelper = new Mock<GenerateErrorHelper>();
            _options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase("TestXReport")
                .Options;
            _httpService.Setup(h => h.Post(It.IsAny<object>())).Returns(GetResponseOfd());
            _context = new ApplicationContext(_options);
            _userManagerMock.Setup(r => r.Users).Returns(GetUsers());
            _controller = new XReportController(_context, _userManagerMock.Object, _errorHelper.Object,
                _httpService.Object);
            _userManagerMock.Setup(u => u.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(GetUser());
            _context.Kkms.AddRangeAsync(GetKkms());
            _context.Shifts.AddRangeAsync(GetShifts());
            _context.Operations.AddRangeAsync(GetOperation());
            _context.SaveChangesAsync();
        }
        
        private async Task<dynamic> GetResponseOfd()
        {
            string path = @"TestControllers/Files/ZXReport.json";
            string json = await File.ReadAllTextAsync(path);
            var ofdResp = JsonConvert.DeserializeObject<CloseShiftOfdResponse>(json);
            string result = JsonConvert.SerializeObject(ofdResp);
            return new JsonResult(result).Value;
        }
        
        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        [TestCase("sd", "1234567")]
        public void TestPostMethod(string Token, string Cashbox)
        {
            var result = (OkObjectResult) _controller.
                Post(new KkmRequest{Token = Token, CashboxUniqueNumber = Cashbox}).Result;
            CloseShiftOfdResponse answer =
                JsonConvert.DeserializeObject<CloseShiftOfdResponse>(result.Value.ToString());

            Assert.IsNotNull(answer);
        }

        private List<Operation> GetOperation()
        {
            return new List<Operation>
            {
                new Operation
                {
                    Amount = 125,
                    Id = "1",
                    Type = OperationTypeEnum.OPERATION_BUY,
                    CashAmount = 15,
                    KkmId = "1",
                    UserId = "1",
                    ShiftId = "1",
                    FiscalNumber = "123456",
                    CheckNumber = 1,
                    CreationDate = DateTime.Now
                },
                new Operation
                {
                    Amount = 125,
                    Id = "2",
                    Type = OperationTypeEnum.OPERATION_BUY,
                    CashAmount = 15,
                    KkmId = "1",
                    UserId = "1",
                    ShiftId = "1",
                    FiscalNumber = "123456",
                    CheckNumber = 1,
                    CreationDate = DateTime.Now
                },
                new Operation
                {
                    Amount = 125,
                    Id = "3",
                    Type = OperationTypeEnum.OPERATION_BUY,
                    CashAmount = 15,
                    KkmId = "1",
                    UserId = "1",
                    ShiftId = "1",
                    FiscalNumber = "123456",
                    CheckNumber = 1,
                    CreationDate = DateTime.Now
                }
            };
        }

        private IQueryable<User> GetUsers()
        {
            List<User> users = new List<User>
            {
                new User
                {
                    Email = "admin@gmail.com",
                    Id = Guid.NewGuid().ToString()
                },
                new User
                {
                    Email = "nurs@gmail.com",
                    Id = Guid.NewGuid().ToString()
                },
                new User
                {
                    Email = "ibrahim@gmail.com",
                    Id = Guid.NewGuid().ToString()
                },
                new User
                {
                    Id = "5",
                    UserName = "Nurs",
                    Address = "Abay",
                    Email = "admin@gmail.com",
                    Fio = "Ibrahim",
                    Inn = "777",
                    Okved = "0",
                    Title = "Title",
                    KkmId = "5",
                    VAT = true,
                    VATSeria = "123456",
                    VATNumber = "1235678",
                    OwnerId = "5",
                    OperatorCode = 1,
                    TaxationType = TaxationTypeEnum.RTS,
                    UserToken = "sss"
                }
            };
            users.Add(GetUser());
            return users.AsQueryable();
        }

        private IQueryable<Shift> GetShifts()
        {
            List<Shift> shifts = new List<Shift>
            {
                new Shift
                {
                    Id = "1",
                    UserId = "1",
                    KkmId = "1",
                    OpenDate = DateTime.Now.AddDays(-1),
                    CloseDate = DateTime.MinValue,
                    Number = 1,
                    BuySaldoBegin = 15,
                    BuySaldoEnd = 25,
                    KkmBalance = 250,
                    SellSaldoBegin = 15,
                    SellSaldoEnd = 25
                },
                new Shift
                {
                    Id = "2",
                    UserId = "5",
                    KkmId = "5",
                    OpenDate = DateTime.Now.AddDays(-1),
                    CloseDate = DateTime.Now,
                    Number = 1,
                    BuySaldoBegin = 15,
                    BuySaldoEnd = 25,
                    KkmBalance = 250,
                    SellSaldoBegin = 15,
                    SellSaldoEnd = 25
                },
            };
            shifts.Add(GetLastShift());
            return shifts.AsQueryable();
        }

        private User GetUser()
        {
            return new User
            {
                Id = "1",
                UserName = "admin",
                Address = "Abay",
                Email = "admin@gmail.com",
                Fio = "Ibrahim",
                Inn = "777",
                Okved = "0",
                Title = "Title",
                KkmId = "1",
                VAT = true,
                VATSeria = "123456",
                VATNumber = "1235678",
                OwnerId = "1",
                OperatorCode = 1,
                TaxationType = TaxationTypeEnum.RTS,
                UserToken = "sd"
            };
        }

        private Shift GetLastShift()
        {
            return new Shift
            {
                Id = "3",
                UserId = "1",
                KkmId = "1",
                OpenDate = DateTime.Now.AddDays(-1),
                CloseDate = DateTime.MinValue,
                Number = 1,
                BuySaldoBegin = 15,
                BuySaldoEnd = 25,
                KkmBalance = 250,
                SellSaldoBegin = 15,
                SellSaldoEnd = 25
            };
        }

        private IQueryable<Kkm> GetKkms()
        {
            List<Kkm> kkm = new List<Kkm>
            {
                new Kkm
                {
                    Address = "Admin",
                    Description = "New",
                    Id = "1",
                    Iin = "7777777777",
                    Mode = "online",
                    Name = "New",
                    User = new User(),
                    CurrentStatus = "online",
                    DeviceId = 2731,
                    IdentificationNumber = "128838383",
                    NameOrg = "FinTeh",
                    OfdToken = 9999999,
                    OperatorId = "1",
                    ReqNum = 123,
                    SerialNumber = "12345",
                    TerminalNumber = "fsdfsf",
                    UserId = "1",
                    FnsKkmId = "12345",
                    LastDateOperation = DateTime.Now,
                    PointOfPayment = "1234"
                },
                new Kkm
                {
                    Address = "NursMan",
                    Description = "Legendary FrontMan",
                    Id = "5",
                    Iin = "666666",
                    Mode = "online",
                    Name = "New",
                    User = new User(),
                    CurrentStatus = "online",
                    DeviceId = 2731,
                    IdentificationNumber = "1288283",
                    NameOrg = "FinTeh",
                    OfdToken = 9999999,
                    OperatorId = "5",
                    ReqNum = 125,
                    SerialNumber = "1234567",
                    TerminalNumber = "fsdfsf111",
                    UserId = "5",
                    FnsKkmId = "1234522",
                    LastDateOperation = DateTime.Now,
                    PointOfPayment = "12345"
                }
            };
            return kkm.AsQueryable();
        }
    }
}