using System;
using System.Collections.Generic;
using System.Linq;
using HSCFiscalRegistrar.Controllers;
using HSCFiscalRegistrar.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.DTO.Auth;
using Models.DTO.UserModel;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using TestProject1.FakeTestModels.Tests;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace TestProject1.TestAuthorizeController
{
    public class TestAuthorizeController
    {
        private string _login = "admin@gmail.com";
        private string _password = "1234567";
        private Mock<FakeUserManager> _userManagerMock;
        private Mock<FakeSignInManager> _signInManagerMock;
        private Mock<GenerateUserToken> _generateUserToken;
        private AuthorizeController _controller;

        [SetUp]
        public void Setup()
        {
            _userManagerMock = new Mock<FakeUserManager>();
            _signInManagerMock = new Mock<FakeSignInManager>();
            _generateUserToken = new Mock<GenerateUserToken>();
            _userManagerMock.Setup(r => r.Users).Returns(GetUsers());
            _signInManagerMock.Setup(s => s.PasswordSignInAsync(
                    _login, _password, false, false))
                .ReturnsAsync(SignInResult.Success);
            _userManagerMock.Setup(x => x.UpdateAsync(It.IsAny<User>()))
                .ReturnsAsync(IdentityResult.Success);
            _controller = new AuthorizeController(_userManagerMock.Object, _signInManagerMock.Object, _generateUserToken.Object);
        }
        
        [Test]
        public void TestToken()
        {
            IQueryable<User> users = GetUsers();
            string token = _generateUserToken.Object.Token(users.ToList()[1].Id);
            string[] stringArray = token.Split('%');
            Assert.That(stringArray.Length, Is.EqualTo(2));
            Guid guid = Guid.Parse(stringArray[0]);
            Assert.That(guid, Is.TypeOf<Guid>());
            Assert.That(token, Is.Not.Null); 
        }

        [Test]
        public void TestAuthorize()
        {
            OkObjectResult result = (OkObjectResult) _controller.Post(new UserDTO {Login = _login, Password = _password}).Result;
            AnswerServerAuth answer = JsonConvert.DeserializeObject<AnswerServerAuth>(result.Value.ToString());
            Assert.True(!string.IsNullOrEmpty(answer.Data.Token));
        }
        
        
        private IQueryable<User> GetUsers()
        {
            List<User> users = new List<User>
            {
                new User
                {
                    Email = "admin@gmail.com",
                    Id = Guid.NewGuid().ToString()
                },
                new User
                {
                    Email = "nurs@gmail.com",
                    Id = Guid.NewGuid().ToString()
                },
                new User
                {
                    Email = "ibrahim@gmail.com",
                    Id = Guid.NewGuid().ToString()
                }
            };
            return users.AsQueryable();
        }
    }
}