﻿using System;
using HSCFiscalRegistrar;
using HSCFiscalRegistrar.Controllers;
using HSCFiscalRegistrar.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.DTO.Cashboxes;
using Models.Enums;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using TestProject1.FakeTestModels.Tests;

namespace TestProject1.TestCashboxesController
{
    [Category("Cashboxes")]
    [TestFixture]
    public class TestCashboxesController
    {
        private Mock<FakeUserManager> _userManagerMock;
        private Mock<TokenValidationHelper> _validationHelper;
        private Mock<GenerateErrorHelper> _errorHelper;
        private DbContextOptions<ApplicationContext> _options;
        private ApplicationContext _context;

        [SetUp]
        public void Setup()
            {
                _options = new DbContextOptionsBuilder<ApplicationContext>()
                    .UseInMemoryDatabase(databaseName: "Kkms1")
                    .Options;
                
                _context = new ApplicationContext(_options);
                TimeSpan time = new TimeSpan(0, 10, 0, 0);
                _context.Users.Add(new User()
                {
                    Address = "Aytieva 57",
                    Email = "nursultan.orynbayev@gmail.com",
                    Fio = "Nursultan Orynbayev",
                    Id = "156-156-156",
                    Inn = "930603302197",
                    Okved = "nnn",
                    Title = "dsdsdsds",
                    EmailConfirmed = true,
                    ExpiryDate = DateTime.Now.Add(time),
                    KkmId = "Nurs",
                    VATSeria = "Dsdsd",
                    VATNumber = "dsdsdsd",
                    DateTimeCreationToken = DateTime.Now,
                    VAT = true,
                    TaxationType = TaxationTypeEnum.RTS,
                    OperatorCode = 15,
                    OwnerId = "156-156-156",
                    UserType = UserTypeEnum.TYPE_ADMIN,
                    UserName = "Dsdsdsds",
                    UserToken = "156-156-156%visualstudiogovno",
                });
                
                _context.Kkms.Add(new Kkm()
                {
                    Address = "Dsdsdsd",
                    Description = "Dsdsdsds",
                    Id = "Nurs",
                    Iin = "dsdsdsdsds",
                    Mode = "Dsdsds",
                    Name = "Dsdsdsd",
                    CurrentStatus = "dsddsd",
                    DeviceId = 15,
                    PointOfPayment = "dsdsds",
                    LastDateOperation = DateTime.Now,
                    FnsKkmId = "dsdsd",
                    UserId = "55555",
                    TerminalNumber = "dsdsds",
                    SerialNumber = "55555",
                    ReqNum = 5,
                    OperatorId = "55555",
                    IdentificationNumber = "dsdsds",
                    NameOrg = "dsdsdsd",
                    OfdToken = 1115151
                });
                
                _context.SaveChanges();
                _userManagerMock = new Mock<FakeUserManager>();
                _userManagerMock.Setup(r => r.Users).Returns(_context.Users);
                _validationHelper = new Mock<TokenValidationHelper>();
                _errorHelper = new Mock<GenerateErrorHelper>();
            }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void GetWrapper_ForResponseCashboxes_SuccesObj()
        {
            DtoToken dto = new DtoToken() {Token = "156-156-156%visualstudiogovno"};
            
            CashboxesController controller = new CashboxesController(
                _validationHelper.Object,
                _errorHelper.Object,
                _userManagerMock.Object,
                _context);
            
            var response = (OkObjectResult) controller.Get(dto);

            Wrapper wrapper = JsonConvert.DeserializeObject<Wrapper>(response.Value.ToString());

            Assert.That(wrapper.Data.List[0].Name, Is.TypeOf<string>());
            Assert.That(wrapper.Data.List[0].RegistrationNumber, Is.TypeOf<string>());
            Assert.That(wrapper.Data.List[0].UniqueNumber, Is.TypeOf<string>());
            Assert.That(wrapper.Data.List[0].IdentificationNumber, Is.TypeOf<string>());
            Assert.That(wrapper.Data.List[0].Shift, Is.TypeOf<int>());
            Assert.That(wrapper.Data.List[0].CurrentStatus, Is.TypeOf<int>());
            
            Assert.That(wrapper.Data.List[0].Name, Is.Not.Null);
            Assert.That(wrapper.Data.List[0].RegistrationNumber, Is.Not.Null);
            Assert.That(wrapper.Data.List[0].UniqueNumber, Is.Not.Null);
            Assert.That(wrapper.Data.List[0].IdentificationNumber, Is.Not.Null);
            Assert.That(wrapper.Data.List[0].Shift, Is.Not.Null);
            Assert.That(wrapper.Data.List[0].CurrentStatus,  Is.Not.Null);
        }
    }
}