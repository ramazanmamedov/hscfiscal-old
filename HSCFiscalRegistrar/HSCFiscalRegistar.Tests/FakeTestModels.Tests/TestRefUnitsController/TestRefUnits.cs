using System.Collections.Generic;
using HSCFiscalRegistrar;
using HSCFiscalRegistrar.Controllers;
using HSCFiscalRegistrar.Directories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace TestProject1.FakeTestModels.Tests.TestRefUnitsController
{
    [TestFixture]
    public class TestRefUnits
    {
        private DbContextOptions<FakeBdContext> _options;
        private FakeBdContext _context;
        private RefUnits _controller;
        
        [SetUp]
        public void Setup()
        {
            _options = new DbContextOptionsBuilder<FakeBdContext>()
                .UseInMemoryDatabase("TestRefUnits")
                .Options;
            _context = new FakeBdContext(_options);
            _controller = new RefUnits();
            _context.unitData.AddRangeAsync(GetUnits());

        }

        [Test]
        [TestCase(1)]
        public void TestMethod(int code)
        {
            var result = (OkObjectResult)_controller.Post();
            var answer = JsonConvert.DeserializeObject<Unit>(result.Value.ToString());
            Assert.IsNotNull(answer);
        }

        private List<UnitData> GetUnits()
        {
            List<UnitData> unitData = new List<UnitData>
            {
                new UnitData
                {
                    Code = 1,
                    NameRu = "шт",
                    NameEn = "PC",
                    NameKz = "дана"

                },new UnitData
                {
                    Code = 796,
                    NameRu = "штанга",
                    NameEn = "Shtanga",
                    NameKz = "штанха"

                },new UnitData
                {
                    Code = 796,
                    NameRu = "штуг",
                    NameEn = "Shtug",
                    NameKz = "штух"

                },
            };
            return unitData;
        }
    }
}