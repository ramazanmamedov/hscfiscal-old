using HSCFiscalRegistrar.Directories;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace TestProject1.FakeTestModels.Tests.TestRefUnitsController
{
    public class FakeBdContext : DbContext
    {
        public DbSet<UnitData> unitData { get; set; }
        
        public FakeBdContext(DbContextOptions<FakeBdContext> options)
            : base(options)
        {
        }
    }
}