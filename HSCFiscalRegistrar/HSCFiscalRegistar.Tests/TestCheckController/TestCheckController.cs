﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HSCFiscalRegistrar;
using HSCFiscalRegistrar.Controllers;
using HSCFiscalRegistrar.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Models.DTO.Fiscalization.KKM;
using Models.DTO.Fiscalization.KKMResponce;
using Models.DTO.Fiscalization.OFDResponse;
using Models.Enums;
using Models.Services;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using TestProject1.FakeTestModels.Tests;

namespace TestProject1.TestCheckController
{
    public class TestCheckController
    {
        private CheckController _controller;
        private Mock<FakeUserManager> _userManagerMock;
        private ApplicationContext _context;
        private Mock<TokenValidationHelper> _helper;
        private Mock<GenerateErrorHelper> _errorHelper;
        private DbContextOptions<ApplicationContext> _options;
        private Mock<IHttpService> _httpService;

        [SetUp]
        public void Setup()
        {
            _httpService = new Mock<IHttpService>();
            _userManagerMock = new Mock<FakeUserManager>();
            _helper = new Mock<TokenValidationHelper>();
            _errorHelper = new Mock<GenerateErrorHelper>();
            _options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase("TestCheckBase")
                .Options;
            _httpService.Setup(h => h.Post(It.IsAny<object>())).Returns(GetOfdResponse());
            _context = new ApplicationContext(_options);
            _userManagerMock.Setup(r => r.Users).Returns(GetUsers());
            _controller = new CheckController(_context, _userManagerMock.Object,
                _helper.Object, _errorHelper.Object, _httpService.Object);
            _userManagerMock.Setup(u => u.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(GetUser());
            _context.Kkms.Add(GetKkm());
            _context.SaveChangesAsync();
        }

        private async Task<dynamic> GetOfdResponse()
        {
            string path = @"TestCheckController/Files/CheckController.json";
            string json = await File.ReadAllTextAsync(path);
            var ofdResp = JsonConvert.DeserializeObject<OfdFiscalResponse>(json);
            string result = JsonConvert.SerializeObject(ofdResp);
            return new JsonResult(result).Value;
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void TestResponseMethod()
        {
            
            OkObjectResult result = (OkObjectResult) _controller.Post(GetCheckOperationRequest()).Result;
            KkmResponse resp = JsonConvert.DeserializeObject<KkmResponse>(result.Value.ToString());
            Assert.NotNull(resp);
            Assert.NotNull(resp.Data.Cashbox);
            Assert.NotNull(resp.Data.CheckNumber);
            Assert.NotNull(resp.Data.DateTime);
            Assert.NotNull(resp.Data.EmployeeName);
            Assert.NotNull(resp.Data.OfflineMode);
            Assert.NotNull(resp.Data.ShiftNumber);
            Assert.NotNull(resp.Data.TicketUrl);
            Assert.NotNull(resp.Data.CashboxOfflineMode);
            Assert.NotNull(resp.Data.CheckOrderNumber);
        }

        private IQueryable<User> GetUsers()
        {
            List<User> users = new List<User>
            {
                new User {Email = "admin@gmail.com", Id = Guid.NewGuid().ToString()},
                new User {Email = "nurs@gmail.com", Id = Guid.NewGuid().ToString()},
                new User {Email = "ibrahim@gmail.com", Id = Guid.NewGuid().ToString()},
                GetUser()
            };
            return users.AsQueryable();
        }


        private User GetUser()
        {
            return new User
            {
                Id = "1",
                UserName = "admin",
                Address = "Abay",
                Email = "admin@gmail.com",
                Fio = "Ibrahim",
                Inn = "777",
                Okved = "0",
                Title = "Title",
                KkmId = "1",
                VAT = true,
                VATSeria = "123456",
                VATNumber = "1235678",
                OwnerId = "1",
                OperatorCode = 1,
                TaxationType = TaxationTypeEnum.RTS
            };
        }

        private Kkm GetKkm()
        {
            return new Kkm
            {
                Address = "Abay",
                Description = "New",
                Id = "1",
                Iin = "7777777777",
                Mode = "online",
                Name = "New",
                User = new User(),
                CurrentStatus = "online",
                DeviceId = 2731,
                IdentificationNumber = "128838383",
                NameOrg = "FinTeh",
                OfdToken = 9999999,
                OperatorId = "1",
                ReqNum = 123,
                SerialNumber = "12345",
                TerminalNumber = "fsdfsf",
                UserId = "1",
                FnsKkmId = "12345",
                LastDateOperation = DateTime.Now,
                PointOfPayment = "1234"
            };
        }

        private CheckOperationRequest GetCheckOperationRequest()
        {
            return new CheckOperationRequest
            {
                Change = 0,
                Payments = GetPaymentsTypeList(),
                Positions = GetPositionTypeList(),
                Token = "ofdtoken",
                CustomerEmail = "admin@gmail.com",
                OperationType = OperationTypeEnum.OPERATION_SELL,
                RoundType = 1,
                TicketModifiers = GetTicketModifiersList(),
                CashboxUniqueNumber = "12345",
                ExternalCheckNumber = "123"
            };
        }

        private List<TicketModifierType> GetTicketModifiersList()
        {
            List<TicketModifierType> ticketList = new List<TicketModifierType>
            {
                new TicketModifierType
                {
                    Sum = 0,
                    Tax = 0,
                    Text = "1",
                    Type = 0,
                    TaxType = TaxationTypeEnum.RTS
                }
            };
            return ticketList;
        }

        private List<PositionType> GetPositionTypeList()
        {
            List<PositionType> positions = new List<PositionType>
            {
                new PositionType
                {
                    Count = 4,
                    Discount = 0,
                    Markup = 0,
                    Price = 100,
                    Tax = 0,
                    DiscountDeleted = true,
                    IsStorno = false,
                    MarkupDeleted = true,
                    PositionCode = "1",
                    PositionName = "hleb",
                    SectionCode = "asd",
                    TaxType = 1,
                    UnitCode = 10
                }
            };
            return positions;
        }

        private List<PaymentsType> GetPaymentsTypeList()
        {
            List<PaymentsType> types = new List<PaymentsType>
            {
                new PaymentsType {Sum = 100, PaymentType = PaymentTypeEnum.PAYMENT_CASH},
                new PaymentsType {Sum = 150, PaymentType = PaymentTypeEnum.PAYMENT_CARD}
            };
            return types;
        }
    }
}